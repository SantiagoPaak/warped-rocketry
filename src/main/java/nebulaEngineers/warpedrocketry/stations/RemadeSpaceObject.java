package nebulaEngineers.warpedrocketry.stations;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import zmaster587.advancedRocketry.api.Configuration;
import zmaster587.advancedRocketry.api.dimension.IDimensionProperties;
import zmaster587.advancedRocketry.api.stations.ISpaceObject;
import zmaster587.advancedRocketry.api.stations.IStorageChunk;
import zmaster587.advancedRocketry.tile.station.TileDockingPort;
import zmaster587.advancedRocketry.util.StationLandingLocation;
import zmaster587.libVulpes.block.BlockFullyRotatable;
import zmaster587.libVulpes.util.HashedBlockPosition;

import java.util.*;

public class RemadeSpaceObject implements ISpaceObject {
    private int Id;
    private int orbitingPlanetId;
    HashedBlockPosition pos;
    private boolean created;
    private HashMap<HashedBlockPosition, String> dockingPoints;
    private List<StationLandingLocation> spawnLocations;

    public RemadeSpaceObject() {
        spawnLocations = new LinkedList<StationLandingLocation>();
        dockingPoints = new HashMap<HashedBlockPosition, String>();
        created = false;
    }

    public void setLandingPadAutoLandStatus(BlockPos pos, boolean status) {
        setLandingPadAutoLandStatus(pos.getX(), pos.getZ(), status);
    }

    public void setLandingPadAutoLandStatus(int x, int z, boolean status) {
        HashedBlockPosition pos = new HashedBlockPosition(x, 0, z);

        Iterator<StationLandingLocation> itr = spawnLocations.iterator();

        while(itr.hasNext()) {
            StationLandingLocation loc = itr.next();
            if(loc.getPos().equals(pos))
                loc.setAllowedForAutoLand(status);
        }
    }

    public void addLandingPad(int x, int z, String name) {
        StationLandingLocation pos = new StationLandingLocation(new HashedBlockPosition(x, 0, z), name);
        if(!spawnLocations.contains(pos)) {
            spawnLocations.add(pos);
            pos.setOccupied(false);
        }
    }

    public void addLandingPad(BlockPos pos, String name) {
        addLandingPad(pos.getX(), pos.getZ(), name);
    }

    public void removeLandingPad(BlockPos pos) {
        removeLandingPad(pos.getX(), pos.getZ());
    }

    /**
     * Removes an existing landing pad from the station
     * @param x
     * @param z
     */
    public void removeLandingPad(int x, int z) {
        HashedBlockPosition pos = new HashedBlockPosition(x, 0, z);

        Iterator<StationLandingLocation> itr = spawnLocations.iterator();

        while(itr.hasNext()) {
            StationLandingLocation loc = itr.next();
            if(loc.getPos().equals(pos))
                itr.remove();
        }
        //spawnLocations.remove(pos);
    }

    /**
     * ID of the station
     * @return This station ID
     */
    @Override
    public int getId() {

        return this.Id;
    }

    /**
     * Used for render and fuel calculations. TODO: Check again when the stations are in WD space and can get location in dimension.
     * @return 0
     */
    @Override
    public float getOrbitalDistance() {

        return 0;
    }

    /**
     * Used for render and fuel calculations. TODO: Check again when the stations are in WD space and can get location in dimension.
     */
    @Override
    public void setOrbitalDistance(float finalVel) {

    }

    /**
     * Propieties of the AR Space Dimension. TODO: Check what attributes are needed
     * @return null
     */
    @Override
    public IDimensionProperties getProperties() {

        return null;
    }

    /**
     * DimID of the planet that is orbiting.
     * @return
     */
    @Override
    public int getOrbitingPlanetId() {

        return this.orbitingPlanetId;
    }

    /**
     *
     * @param id the space object id of this object (NOT DIMID)
     */
    @Override
    public void setId(int id) {
        this.Id=id;
    }

    /**
     * Position in space. Redundant with SpawnLocation
     * @param posX
     * @param posY
     */
    @Override
    public void setPos(int posX, int posY) {

    }


    /**
     * Change position where the station first appeared, also used as position in space. Only used by SpaceObjectManager
     * @param x
     * @param y
     * @param z
     */
    @Override
    public void setSpawnLocation(int x, int y, int z) {
        this.pos = new HashedBlockPosition(x,y,z);
    }

    /**
     * Inform of the station in what planet is it
     * @param id
     */
    @Override
    public void setOrbitingBody(int id) {
        this.orbitingPlanetId=id; //TODO: Maybe add some check if the planet exists
    }


    @Override
    public HashedBlockPosition getSpawnLocation() {
        return null;
    }

    /**
     * If false, SpaceObjectManager changes spawn position based on config. Probably must always start as true
     * @return
     */
    @Override
    public boolean hasCustomSpawnLocation() {
        return false;
    }

    /**
     * Handles the construction of new ships and adding new modules. I believe it works like this just fine
     * @param chunk
     */
    @Override
    public void onModuleUnpack(IStorageChunk chunk) {
        if(DimensionManager.isDimensionRegistered(Configuration.spaceDimId) &&  DimensionManager.getWorld(Configuration.spaceDimId) == null) //Check if WD dimension has been loaded
            DimensionManager.initDimension(Configuration.spaceDimId); //Loads WD dimension if not
        World worldObj = DimensionManager.getWorld(Configuration.spaceDimId); //Gets WD dimension
        if(!created) { //If the station hasn't been created
            chunk.pasteInWorld(worldObj, this.pos.x - chunk.getSizeX()/2, this.pos.y - chunk.getSizeY()/2, this.pos.z - chunk.getSizeZ()/2); //Paste the station

            created = true;
            //setLaunchPos((int)posX, (int)posZ);
            //setPos((int)posX, (int)posZ);
        }
        else {
            List<TileEntity> tiles = chunk.getTileEntityList(); //Get all tiles in station
            List<String> targetIds = new LinkedList<String>();
            List<TileEntity> myPoss = new LinkedList<TileEntity>();
            HashedBlockPosition pos = null;
            TileDockingPort destTile = null;
            TileDockingPort srcTile = null;

            for(TileEntity tile : tiles) {
                if(tile instanceof TileDockingPort) {
                    targetIds.add(((TileDockingPort)tile).getTargetId());
                    myPoss.add(tile);
                }
            }

            //Find the first docking port on the station that matches the id in the new chunk
            for(Map.Entry<HashedBlockPosition, String> map : dockingPoints.entrySet()) {
                if(targetIds.contains(map.getValue())) {
                    int loc = targetIds.indexOf(map.getValue());
                    pos = map.getKey();
                    TileEntity tile;
                    if((tile = worldObj.getTileEntity(pos.getBlockPos())) instanceof TileDockingPort) {
                        destTile = (TileDockingPort)tile;
                        srcTile = (TileDockingPort) myPoss.get(loc);
                        break;
                    }
                }
            }

            if(destTile != null) {
                EnumFacing stationFacing = destTile.getBlockType().getStateFromMeta(destTile.getBlockMetadata()).getValue(BlockFullyRotatable.FACING);
                EnumFacing moduleFacing = srcTile.getBlockType().getStateFromMeta(srcTile.getBlockMetadata()).getValue(BlockFullyRotatable.FACING);


                EnumFacing cross = moduleFacing.rotateAround(stationFacing.getAxis());

                if(stationFacing.getAxisDirection() == EnumFacing.AxisDirection.NEGATIVE)
                    cross = cross.getOpposite();

                if(cross == moduleFacing) {
                    if(moduleFacing == stationFacing) {
                        if(cross == EnumFacing.DOWN || cross == EnumFacing.UP) {
                            chunk.rotateBy(EnumFacing.NORTH);
                            chunk.rotateBy(EnumFacing.NORTH);
                        }
                        else {
                            chunk.rotateBy(EnumFacing.UP);
                            chunk.rotateBy(EnumFacing.UP);
                        }
                    }
                }
                else if(cross.getOpposite() != moduleFacing)
                    chunk.rotateBy(stationFacing.getFrontOffsetY() == 0 ? cross : cross.getOpposite());

                int xCoord = (stationFacing.getFrontOffsetX() == 0 ? -srcTile.getPos().getX() : srcTile.getPos().getX()*stationFacing.getFrontOffsetX()) + stationFacing.getFrontOffsetX() + destTile.getPos().getX();
                int yCoord = (stationFacing.getFrontOffsetY() == 0 ? -srcTile.getPos().getY() : srcTile.getPos().getY()*stationFacing.getFrontOffsetY()) + stationFacing.getFrontOffsetY() + destTile.getPos().getY();
                int zCoord = (stationFacing.getFrontOffsetZ() == 0 ? -srcTile.getPos().getZ() : srcTile.getPos().getZ()*stationFacing.getFrontOffsetZ()) + stationFacing.getFrontOffsetZ() + destTile.getPos().getZ();
                chunk.pasteInWorld(worldObj, xCoord, yCoord, zCoord);
                worldObj.setBlockToAir(destTile.getPos().offset(stationFacing));
                worldObj.setBlockToAir(destTile.getPos());
            }
        }
    }

    /**
     * Internal Forge thing, check with documentation
      * @param nbt
     */
    @Override
    public void writeToNbt(NBTTagCompound nbt) {

    }

    /**
     * Internal Forge thing, check with documentation
     * @param nbt
     */
    @Override
    public void readFromNbt(NBTTagCompound nbt) {

    }

    /**
     * Client sky rendering stuff
     * @param dir
     * @return
     */
    @Override
    public double getRotation(EnumFacing dir) {
        return 0;
    }

    /**
     * Client sky rendering stuff
     * @param dir
     * @return
     */
    @Override
    public double getDeltaRotation(EnumFacing dir) {
        return 0;
    }

    /**
     * Client sky rendering stuff
     * @param rotation
     * @param dir
     * @return
     */
    @Override
    public void setRotation(double rotation, EnumFacing dir) {

    }

    /**
     * Client sky rendering stuff
     * @return
     */
    @Override
    public double getMaxRotationalAcceleration() {
        return 0;
    }

    /**
     * Client sky rendering stuff
     * @param rotation
     * @param dir
     */
    @Override
    public void setDeltaRotation(double rotation, EnumFacing dir) {

    }

    /**
     * Checks for free landing pads
     * @return
     */
    @Override
    public boolean hasFreeLandingPad() {
        return false;
    }

    /**
     * Search for points to land
     * @param commit
     * @return
     */
    @Override
    public HashedBlockPosition getNextLandingPad(boolean commit) {
        return null;
    }



    /**
     * Checks if a specific pad is aviable
     * @param posX
     * @param posZ
     * @param full true if the pad is avalible to use
     */
    @Override
    public void setPadStatus(int posX, int posZ, boolean full) {

    }

    /**
     * WarpCore stuff
     * @param time time in ticks
     */
    @Override
    public void beginTransition(long time) {

    }

    /**
     * WarpCore stuff
     * @return
     */
    @Override
    public long getTransitionTime() {
        return 0;
    }

    /**
     * WarpCore stuff
     * @param id
     */
    @Override
    public void setDestOrbitingBody(int id) {

    }

    /**
     * WarpCore stuff
     * @return
     */
    @Override
    public int getDestOrbitingBody() {
        return 0;
    }

    /**
     * Space Dimension stuff
     * @param properties
     */
    @Override
    public void setProperties(IDimensionProperties properties) {

    }

    /**
     * Temporary space object stuff
     * @return
     */
    @Override
    public long getExpireTime() {
        return 0;
    }

    /**
     * Rendering stuff
     * @return
     */
    @Override
    public EnumFacing getForwardDirection() {
        return null;
    }
}
