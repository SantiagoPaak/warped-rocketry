package nebulaEngineers.warpedrocketry.entity;

//TODO: Change reference to AR world

import net.minecraft.world.World;
import zmaster587.advancedRocketry.api.StatsRocket;
import zmaster587.advancedRocketry.entity.EntityStationDeployedRocket;
import zmaster587.advancedRocketry.util.StorageChunk;

public class WarpedEntityStationDeployedRocket extends EntityStationDeployedRocket {

    public WarpedEntityStationDeployedRocket(World world) {
        super(world);
    }

    public WarpedEntityStationDeployedRocket(World world, StorageChunk storage, StatsRocket stats, double x, double y, double z) {
        super(world, storage, stats, x, y, z);
    }

    @Override
    public void onOrbitReached(){

    }
}